Instafilter an app that lets the user import photos from their library, then modify them using various image effects.

Core Image, UIKit, PhotosUI.


<img src="/uploads/c154a058998080f18f7491f723320193/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-01-27_at_18.57.19.png" width="320" >

<img src="/uploads/136eda087597b3d0f5174590efa20767/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-01-27_at_19.00.21.png" width="320" >

<img src="/uploads/37467a5077dd708bd2c022ef07cbb142/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-01-27_at_18.58.28.png" width="320" >

<img src="/uploads/a1a17219457209841f16c7c23bc1888a/Simulator_Screen_Shot_-_iPhone_14_Pro_-_2023-01-27_at_18.57.54.png" width="320" >
