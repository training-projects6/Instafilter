//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by Artem Soloviev on 09.11.2022.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
